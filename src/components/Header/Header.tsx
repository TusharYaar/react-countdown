import React, { Component } from "react";

import "./Header.css";

export default class Header extends Component<{ clear: () => void; toggleSetting: () => void }> {
  render() {
    return (
      <header className="header-container">
        <h1 className="header-title">Countdown Timer</h1>
        <div className="header-btn-container">
          <button className="header-btn" onClick={this.props.clear}>
            Clear
          </button>
          <button className="header-btn" onClick={this.props.toggleSetting}>
            Settings
          </button>
        </div>
      </header>
    );
  }
}
