import React, { Component } from "react";

import "./DateInput.css";

type Props = {
  updateCountDown: (date: Date) => void;
};

export default class DateInput extends Component<Props> {
  state: { date: Date } = {
    date: new Date(),
  };

  getCurrentDate(date: Date) {
    const year = date.getFullYear();
    const month = date.getMonth() > 8 ? date.getMonth() + 1 : "0" + (date.getMonth() + 1);
    const day = date.getDate() > 9 ? date.getDate() : "0" + date.getDate();
    return `${year}-${month}-${day}`;
  }

  handleDateChange(e: React.ChangeEvent<HTMLInputElement>) {
    this.setState({
      date: new Date(e.target.value),
    });
  }

  handleSetCountdown() {
    this.props.updateCountDown(this.state.date);
  }

  render() {
    return (
      <div className="date-input">
        <input
          className="input"
          type="date"
          value={`${this.getCurrentDate(this.state.date)}`}
          onChange={this.handleDateChange.bind(this)}
        />
        <button onClick={this.handleSetCountdown.bind(this)} className="set-btn">
          Set Countdown
        </button>
      </div>
    );
  }
}
