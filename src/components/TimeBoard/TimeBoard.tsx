import React, { Component } from "react";

import "./TimeBoard.css";

type Props = {
  time: number;
  title: string;
};

export default class TimeBoard extends Component<Props> {
  render() {
    const { time, title } = this.props;
    return (
      <div className="timeboard-container">
        <p className="timeboard-time">{time}</p>
        <p className="timeboard-title">{title}</p>
      </div>
    );
  }
}
