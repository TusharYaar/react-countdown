import React from "react";
import "./App.css";

import Header from "./components/Header/Header";
import TimeBoard from "./components/TimeBoard/TimeBoard";
import DateInput from "./components/DateInput/DateInput";

type State = {
  days: number;
  hours: number;
  minutes: number;
  seconds: number;
  settingVisible: boolean;
};

class App extends React.Component<{}, State> {
  private interval: any;

  state: State = {
    days: 0,
    hours: 0,
    minutes: 0,
    seconds: 0,
    settingVisible: false,
  };

  startTimer(date: Date) {
    if (this.interval) clearInterval(this.interval);
    this.interval = setInterval(() => {
      const now = new Date().getTime();
      const distance = date.getTime() - now;
      const days = Math.floor(distance / (1000 * 60 * 60 * 24));
      const hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
      const minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
      const seconds = Math.floor((distance % (1000 * 60)) / 1000);

      if (distance <= 0 && this.interval) {
        this.setState({
          days: 0,
          hours: 0,
          minutes: 0,
          seconds: 0,
        });
        clearInterval(this.interval);
      } else
        this.setState({
          days,
          hours,
          minutes,
          seconds,
        });
    }, 1000);
  }

  componentDidMount() {
    this.startTimer(new Date(2022, 8, 16));
  }

  componentWillUnmount() {
    if (this.interval) {
      clearInterval(this.interval);
    }
  }

  handleClearTimer() {
    if (this.interval) {
      clearInterval(this.interval);
      this.setState({
        days: 0,
        hours: 0,
        minutes: 0,
        seconds: 0,
      });
    }
  }

  updateCountDown(date: Date) {
    this.startTimer(date);
    this.toggleSettingVisibility();
  }

  toggleSettingVisibility() {
    this.setState({
      settingVisible: !this.state.settingVisible,
    });
  }

  render() {
    return (
      <div className="App">
        <Header clear={this.handleClearTimer.bind(this)} toggleSetting={this.toggleSettingVisibility.bind(this)} />
        {this.state.settingVisible && <DateInput updateCountDown={this.updateCountDown.bind(this)} />}
        <div className="App-body">
          <h3 className="timer-message">Countdown ends in....</h3>
          <div className="time-container">
            <TimeBoard title="Days" time={this.state.days} />
            <TimeBoard title="Hours" time={this.state.hours} />
            <TimeBoard title="Minutes" time={this.state.minutes} />
            <TimeBoard title="Seconds" time={this.state.seconds} />
          </div>
        </div>
      </div>
    );
  }
}

export default App;
